/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

$('form[name="addprototype"], form[name="editprototype"], form[name="addalert"] , form[name="editalert"]').on('submit', (function (e) {
    e.preventDefault();
    $.ajax({
        url: this.getAttribute('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (msg) {
            if (msg.success == true) {
                $("div#msgSuccess").show();
                $("div#msgSuccess p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgSuccess").hide();
                }, 4000);
            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}));

function deletePrototype(id, url) {
    $.ajax({
        url: decodeURIComponent(url),
        type: "POST",
        data: "id=" + id,
        cache: false,
        success: function (msg) {
            if (msg.success == true) {
                $("div#msgSuccess").show();
                $("div#msgSuccess p#text").html(msg.message);
                setTimeout(function () {
                    location.reload();
                    $("div#msgSuccess").hide();
                }, 2000);

            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}

function deleteAlert(id, url) {
    $.ajax({
        url: decodeURIComponent(url),
        type: "POST",
        data: "id=" + id,
        cache: false,
        success: function (msg) {
            if (msg.success == true) {
                $("div#msgSuccess").show();
                $("div#msgSuccess p#text").html(msg.message);
                setTimeout(function () {
                    location.reload();
                    $("div#msgSuccess").hide();
                }, 2000);

            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}

$('select[name="addalert[alertType]"], select[name="editalert[alertType]"]').on('change', (function (e){ if(this.value == 1){ $("div#value2").show(); }else{ $("div#value2").hide(); } }));
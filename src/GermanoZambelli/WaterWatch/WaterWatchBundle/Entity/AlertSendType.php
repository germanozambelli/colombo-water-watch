<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlertSendType
 *
 * @ORM\Table(name="alert_send_type")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\AlertSendTypeRepository")
 */
class AlertSendType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AlertSendType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

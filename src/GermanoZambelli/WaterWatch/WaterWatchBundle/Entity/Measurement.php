<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Measurement
 *
 * @ORM\Table(name="measurement")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\MeasurementRepository")
 */
class Measurement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255)
     */
    private $ip;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * Many Measurament have One Prototype.
     * @ORM\ManyToOne(targetEntity="Prototype")
     * @ORM\JoinColumn(name="prototype_id", referencedColumnName="id")
     */
    private $prototype;

    /**
     * Many Measurament have One Type.
     * @ORM\ManyToOne(targetEntity="MeasurementType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Measurement
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Measurement
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Measurement
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * Set prototype
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype $prototype
     *
     * @return Measurement
     */
    public function setPrototype(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype $prototype = null)
    {
        $this->prototype = $prototype;

        return $this;
    }

    /**
     * Get prototype
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype
     */
    public function getPrototype()
    {
        return $this->prototype;
    }

    /**
     * Set type
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType $type
     *
     * @return Measurement
     */
    public function setType(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType
     */
    public function getType()
    {
        return $this->type;
    }
}

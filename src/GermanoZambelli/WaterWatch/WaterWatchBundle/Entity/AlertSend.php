<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AlertSend
 *
 * @ORM\Table(name="alert_send")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\AlertSendRepository")
 */
class AlertSend
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * Many AlertSend have One Type.
     * @ORM\ManyToOne(targetEntity="AlertSendType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * Many AlertSend have One Measurement.
     * @ORM\ManyToOne(targetEntity="Measurement")
     * @ORM\JoinColumn(name="measurement_id", referencedColumnName="id")
     */
    private $measurement;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return AlertSend
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set type
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertSendType $type
     *
     * @return AlertSend
     */
    public function setType(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertSendType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertSendType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set measurement
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement
     *
     * @return AlertSend
     */
    public function setMeasurement(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement = null)
    {
        $this->measurement = $measurement;

        return $this;
    }

    /**
     * Get measurement
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement
     */
    public function getMeasurement()
    {
        return $this->measurement;
    }
}

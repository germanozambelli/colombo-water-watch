<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Prototype
 *
 * @ORM\Table(name="prototype")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\PrototypeRepository")
 */
class Prototype
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=255, unique=true)
     */
    private $apiKey;

    /**
     * @var float
     *
     * @ORM\Column(name="temp_alert", type="float")
     */
    private $tempAlert;

    /**
     * @var float
     *
     * @ORM\Column(name="hydrometric_alert", type="float")
     */
    private $hydrometricAlert;

    /**
     * @var float
     *
     * @ORM\Column(name="rain_alert", type="float")
     */
    private $rainAlert;

    /**
     * @var float
     *
     * @ORM\Column(name="battery_alert", type="float")
     */
    private $batteryAlert;

    /**
     * One Prototype has Many Measurement.
     * @ORM\OneToMany(targetEntity="Measurement", mappedBy="prototype")
     */
    private $measurements;

    public function __construct() {
        $this->measurements = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Prototype
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return Prototype
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set tempAlert
     *
     * @param float $tempAlert
     *
     * @return Prototype
     */
    public function setTempAlert($tempAlert)
    {
        $this->tempAlert = $tempAlert;

        return $this;
    }

    /**
     * Get tempAlert
     *
     * @return float
     */
    public function getTempAlert()
    {
        return $this->tempAlert;
    }

    /**
     * Set hydrometricAlert
     *
     * @param float $hydrometricAlert
     *
     * @return Prototype
     */
    public function setHydrometricAlert($hydrometricAlert)
    {
        $this->hydrometricAlert = $hydrometricAlert;

        return $this;
    }

    /**
     * Get hydrometricAlert
     *
     * @return float
     */
    public function getHydrometricAlert()
    {
        return $this->hydrometricAlert;
    }

    /**
     * Set rainAlert
     *
     * @param float $rainAlert
     *
     * @return Prototype
     */
    public function setRainAlert($rainAlert)
    {
        $this->rainAlert = $rainAlert;

        return $this;
    }

    /**
     * Get rainAlert
     *
     * @return float
     */
    public function getRainAlert()
    {
        return $this->rainAlert;
    }

    /**
     * Set batteryAlert
     *
     * @param float $batteryAlert
     *
     * @return Prototype
     */
    public function setBatteryAlert($batteryAlert)
    {
        $this->batteryAlert = $batteryAlert;

        return $this;
    }

    /**
     * Get batteryAlert
     *
     * @return float
     */
    public function getBatteryAlert()
    {
        return $this->batteryAlert;
    }

    /**
     * Add measurement
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement
     *
     * @return Prototype
     */
    public function addMeasurement(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement)
    {
        $this->measurements[] = $measurement;

        return $this;
    }

    /**
     * Remove measurement
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement
     */
    public function removeMeasurement(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement)
    {
        $this->measurements->removeElement($measurement);
    }

    /**
     * Get measurements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }
}

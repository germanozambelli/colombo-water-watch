<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;

/**
 * Measurement
 *
 * @ORM\Table(name="alert")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\AlertRepository")
 */
class Alert
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string" , length=50)
     */
    private $value;

    /**
     * Many Measurament have One Prototype.
     * @ORM\ManyToOne(targetEntity="Prototype")
     * @ORM\JoinColumn(name="prototype_id", referencedColumnName="id")
     */
    private $prototype;

    /**
     * Many Alert have One  Measurement Type.
     * @ORM\ManyToOne(targetEntity="MeasurementType")
     * @ORM\JoinColumn(name="measurament_type_id", referencedColumnName="id")
     */
    private $measurementType;

    /**
     * Many Alert have One AlertType.
     * @ORM\ManyToOne(targetEntity="AlertType")
     * @ORM\JoinColumn(name="alert_type_id", referencedColumnName="id")
     */
    private $alertType;

    /**
     * Many Alert have One User.
     * @ORM\ManyToOne(targetEntity="\GermanoZambelli\ZMBManagement\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Alert
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get value
     *
     * @return ArrayCollection
     */
    public function getJsonDecodedValue()
    {
        return json_decode($this->value, true);
    }

    /**
     * Set prototype
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype $prototype
     *
     * @return Alert
     */
    public function setPrototype(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype $prototype = null)
    {
        $this->prototype = $prototype;

        return $this;
    }

    /**
     * Get prototype
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype
     */
    public function getPrototype()
    {
        return $this->prototype;
    }

    /**
     * Set measurementType
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType $measurementType
     *
     * @return Alert
     */
    public function setMeasurementType(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType $measurementType = null)
    {
        $this->measurementType = $measurementType;

        return $this;
    }

    /**
     * Get measurementType
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType
     */
    public function getMeasurementType()
    {
        return $this->measurementType;
    }

    /**
     * Set alertType
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertType $alertType
     *
     * @return Alert
     */
    public function setAlertType(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertType $alertType = null)
    {
        $this->alertType = $alertType;

        return $this;
    }

    /**
     * Get alertType
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertType
     */
    public function getAlertType()
    {
        return $this->alertType;
    }

    /**
     * Set user
     *
     * @param \GermanoZambelli\ZMBManagement\UserBundle\Entity\User $user
     *
     * @return Alert
     */
    public function setUser(\GermanoZambelli\ZMBManagement\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \GermanoZambelli\ZMBManagement\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

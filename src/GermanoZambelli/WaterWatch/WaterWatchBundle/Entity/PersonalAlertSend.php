<?php

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonalAlertSend
 *
 * @ORM\Table(name="personal_alert_send")
 * @ORM\Entity(repositoryClass="GermanoZambelli\WaterWatch\WaterWatchBundle\Repository\PersonalAlertSendRepository")
 */
class PersonalAlertSend
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * Many PersonalAlertSend have One Alert.
     * @ORM\ManyToOne(targetEntity="Alert")
     * @ORM\JoinColumn(name="alert_id", referencedColumnName="id")
     */
    private $alert;

    /**
     * Many PersonalAlertSend have One Measurement.
     * @ORM\ManyToOne(targetEntity="Measurement")
     * @ORM\JoinColumn(name="measurement_id", referencedColumnName="id")
     */
    private $measurement;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return PersonalAlertSend
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set alert
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert $alert
     *
     * @return PersonalAlertSend
     */
    public function setAlert(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert $alert = null)
    {
        $this->alert = $alert;

        return $this;
    }

    /**
     * Get alert
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert
     */
    public function getAlert()
    {
        return $this->alert;
    }

    /**
     * Set measurement
     *
     * @param \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement
     *
     * @return PersonalAlertSend
     */
    public function setMeasurement(\GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement $measurement = null)
    {
        $this->measurement = $measurement;

        return $this;
    }

    /**
     * Get measurement
     *
     * @return \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement
     */
    public function getMeasurement()
    {
        return $this->measurement;
    }
}

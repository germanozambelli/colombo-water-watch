<?php
/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\AlertJob;


use DateTime;

class AlertJobClock
{
    /*
     * rate of iteration in seconds
     */
    const RATE = 2;
    private $lastCheck;

    public function __construct()
    {
        $this->lastCheck = new DateTime("now");
    }

    /**
     * @return bool
     */
    public function isIterable()
    {
        if ((time() - $this->getLastCheck()->getTimestamp()) > $this::RATE) {
            $this->lastCheck = new DateTime("now");
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return DateTime
     */
    public function getLastCheck()
    {
        return $this->lastCheck;
    }

    /**
     * @param \DateTime $lastCheck
     */
    public function setLastCheck($lastCheck)
    {
        $this->lastCheck = $lastCheck;
    }

    public function updateLastCheck()
    {
        $this->lastCheck = new DateTime("now");
    }

}
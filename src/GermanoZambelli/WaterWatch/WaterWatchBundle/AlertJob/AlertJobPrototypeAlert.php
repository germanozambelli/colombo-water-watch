<?php
/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\AlertJob;

use DateTime;
use Doctrine\ORM\EntityManager;

use Facebook\Facebook;
use Facebook\FacebookApp;
use Facebook\FacebookRequest;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertSend;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Services\MailServices;
use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use Symfony\Component\Console\Output\OutputInterface;

class AlertJobPrototypeAlert
{
    /**
     * This var contain the Entity Manager for access to doctrine.
     *
     * @var EntityManager $em
     */
    private $em;

    /**
     * This var contain the OutputInterface.
     *
     * @var OutputInterface $output
     */
    private $output;

    /**
     *
     * @var AlertJobClock $clock
     */
    private $clock;

    /**
     * @var MailServices $mailer
     */
    private $mailer;

    public function __construct(EntityManager $em, OutputInterface $output, AlertJobClock $clock, MailServices $mailer)
    {
        $this->em = $em;
        $this->output = $output;
        $this->clock = $clock;
        $this->mailer = $mailer;
    }

    public function checkMeasurement()
    {
        $this->output->writeln("controllo allerte generali");
        /**
         * @var Prototype[] $prototypes
         */

        $prototypes = $this->em->getRepository('WaterWatchBundle:Prototype')->findAll();

        /**
         * @var Prototype $prototype
         */
        foreach ($prototypes as $prototype) {
            /**
             * @var Measurement $measurement
             */
            $this->em->detach($prototype);
            $measurements = $this->em->getRepository('WaterWatchBundle:Measurement')->findMeasurementByDataAndPrototype($this->clock->getLastCheck(), $prototype->getId());

            foreach ($measurements as $measurement) {

                switch ($measurement->getType()->getName()) {
                    case "battery":
                        $this->checkBatteryValue($prototype, $measurement);
                        break;
                    case "hydrometric":
                        $this->checkHydrometricValue($prototype, $measurement);
                        break;
                    case "rain":
                        $this->checkRainValue($prototype, $measurement);
                        break;
                    case "temp":
                        $this->checkTempValue($prototype, $measurement);
                        break;
                }

            }
        }
    }

    private function checkBatteryValue(Prototype $prototype, Measurement $measurement)
    {
        if ($measurement->getValue() <= $prototype->getBatteryAlert()) {
            $typeSend = $this->em->getRepository('WaterWatchBundle:AlertSendType')->findOneBy(["name" => "email"]);
            $checkSend = $this->em->getRepository('WaterWatchBundle:AlertSend')->findOneBy(["type" => $typeSend->getId(), "measurement" => $measurement->getId()]);
            if (!$checkSend) {
                $users = $this->em->getRepository('UserBundle:User')->findBy(["isAdmin" => "1"]);
                /**
                 * @var User $user
                 */
                foreach ($users as $user) {

                    $this->output->writeln("invio email batteria a " . $user->getUsername());
                    $this->mailer->sendEmail("Valore della batteria basso", $user->getMail(), "WaterWatchBundle:Mail/Alert:battery.html.twig", ["username" => $user->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "min" => $prototype->getBatteryAlert()]);
                }

                $alertSend = new AlertSend();
                $alertSend->setMeasurement($measurement);
                $alertSend->setType($typeSend);
                $alertSend->setData(new DateTime("now"));
                $this->em->persist($alertSend);
                $this->em->flush($alertSend);
            }
        }
    }

    private function checkHydrometricValue(Prototype $prototype, Measurement $measurement)
    {
        if ($measurement->getValue() >= $prototype->getHydrometricAlert()) {
            $typeSend = $this->em->getRepository('WaterWatchBundle:AlertSendType')->findOneBy(["name" => "email"]);

            $checkSend = $this->em->getRepository('WaterWatchBundle:AlertSend')->findOneBy(["type" => $typeSend, "measurement" => $measurement->getId()]);

            if (!$checkSend) {
                $users = $this->em->getRepository('UserBundle:User')->findAll();
                /**
                 * @var User $user
                 */
                foreach ($users as $user) {
                    $this->em->detach($user);
                    $this->output->writeln("invio email valore idrometrico a " . $user->getUsername());
                    $this->mailer->sendEmail("Valore idrometrico alto", $user->getMail(), "WaterWatchBundle:Mail/Alert:hydrometric.html.twig", ["username" => $user->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "max" => $prototype->getHydrometricAlert()]);
                }

                $alertSend = new AlertSend();
                $alertSend->setMeasurement($measurement);
                $alertSend->setType($typeSend);
                $alertSend->setData(new DateTime("now"));
                $this->em->persist($alertSend);
                $this->em->flush($alertSend);
            }
        }
    }

    private function checkRainValue(Prototype $prototype, Measurement $measurement)
    {
        if ($measurement->getValue() >= $prototype->getRainAlert()) {
            $typeSend = $this->em->getRepository('WaterWatchBundle:AlertSendType')->findOneBy(["name" => "email"]);

            $checkSend = $this->em->getRepository('WaterWatchBundle:AlertSend')->findOneBy(["type" => $typeSend, "measurement" => $measurement]);

            if (!$checkSend) {
                $users = $this->em->getRepository('UserBundle:User')->findAll();
                /**
                 * @var User $user
                 */
                foreach ($users as $user) {
                    $this->em->detach($user);
                    $this->output->writeln("invio email valore pioggia a " . $user->getUsername());
                    $this->mailer->sendEmail("Valore pioggia alto", $user->getMail(), "WaterWatchBundle:Mail/Alert:rain.html.twig", ["username" => $user->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "max" => $prototype->getRainAlert()]);
                }
                $alertSend = new AlertSend();
                $alertSend->setMeasurement($measurement);
                $alertSend->setType($typeSend);
                $alertSend->setData(new DateTime("now"));
                $this->em->persist($alertSend);
                $this->em->flush($alertSend);
            }
        }
    }

    private function checkTempValue(Prototype $prototype, Measurement $measurement)
    {
        if ($measurement->getValue() >= $prototype->getRainAlert()) {
            $typeSend = $this->em->getRepository('WaterWatchBundle:AlertSendType')->findOneBy(["name" => "email"]);

            $checkSend = $this->em->getRepository('WaterWatchBundle:AlertSend')->findOneBy(["type" => $typeSend, "measurement" => $measurement]);

            if (!$checkSend) {
                $users = $this->em->getRepository('UserBundle:User')->findAll();
                /**
                 * @var User $user
                 */
                foreach ($users as $user) {
                    $this->em->detach($user);
                    $this->output->writeln("invio email valore temperatura a " . $user->getUsername());
                    $this->mailer->sendEmail("Valore temperatura alto", $user->getMail(), "WaterWatchBundle:Mail/Alert:temp.html.twig", ["username" => $user->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "max" => $prototype->getTempAlert()]);
                }
                $alertSend = new AlertSend();
                $alertSend->setMeasurement($measurement);
                $alertSend->setType($typeSend);
                $alertSend->setData(new DateTime("now"));
                $this->em->persist($alertSend);
                $this->em->flush($alertSend);
            }
        }
    }

}
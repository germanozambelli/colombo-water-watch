<?php
/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\AlertJob;

use Doctrine\ORM\EntityManager;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Services\MailServices;
use Symfony\Component\Console\Output\OutputInterface;

class AlertJob
{
    /**
     * This var contain the Entity Manager for access to doctrine.
     *
     * @var EntityManager $em
     */
    private $em;

    /**
     * This var contain the OutputInterface.
     *
     * @var OutputInterface $output
     */
    private $output;

    /**
     *
     * @var AlertJobClock $clock
     */
    private $clock;

    /**
     * @var MailServices $mailer
     */
    private $mailer;

    /**
     * @var AlertJobPrototypeAlert $prototypeAlert
     */
    private $prototypeAlert;


    public function __construct(EntityManager $em, OutputInterface $output, MailServices $mailer)
    {
        $this->em = $em;
        $this->output = $output;
        $this->mailer = $mailer;
        $this->clock = new AlertJobClock();
        $this->prototypeAlert = new AlertJobPrototypeAlert($this->em, $this->output, $this->clock, $this->mailer);
        $this->personalAlert = new AlertJobPersonalAlert($this->em, $this->output, $this->clock, $this->mailer);
    }

    public function startJob()
    {
        while (true) {
            if ($this->clock->isIterable()) {
                $this->prototypeAlert->checkMeasurement();
                $this->personalAlert->checkMeasurement();
                $this->clock->updateLastCheck();
            }
        }
    }

}
<?php
/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\AlertJob;

use DateTime;
use Doctrine\ORM\EntityManager;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertSend;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\PersonalAlertSend;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Services\MailServices;
use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use Symfony\Component\Console\Output\OutputInterface;

class AlertJobPersonalAlert
{
    /**
     * This var contain the Entity Manager for access to doctrine.
     *
     * @var EntityManager $em
     */
    private $em;

    /**
     * This var contain the OutputInterface.
     *
     * @var OutputInterface $output
     */
    private $output;

    /**
     *
     * @var AlertJobClock $clock
     */
    private $clock;

    /**
     * @var MailServices $mailer
     */
    private $mailer;

    public function __construct(EntityManager $em, OutputInterface $output, AlertJobClock $clock, MailServices $mailer)
    {
        $this->em = $em;
        $this->output = $output;
        $this->clock = $clock;
        $this->mailer = $mailer;
    }

    public function checkMeasurement()
    {
        $this->output->writeln("controllo allerte personali");
        /**
         * @var Prototype[] $prototypes
         */

        $prototypes = $this->em->getRepository('WaterWatchBundle:Prototype')->findAll();
        /**
         * @var Prototype $prototype
         */

        foreach ($prototypes as $prototype) {
            /**
             * @var Measurement $measurement
             *
             */
            $measurements = $this->em->getRepository('WaterWatchBundle:Measurement')->findMeasurementByDataAndPrototype($this->clock->getLastCheck(), $prototype->getId());
            foreach ($measurements as $measurement) {
                $this->checkValue($measurement->getPrototype(), $measurement);
            }
        }
    }


    private function checkValue(Prototype $prototype, Measurement $measurement)
    {

        $alerts = $this->em->getRepository('WaterWatchBundle:Alert')->findBy(["prototype" => $prototype->getId(), "measurementType" => $measurement->getType()->getId()]);
        /**
         * @ Alert $alert
         */
        foreach ($alerts as $alert) {
            if ($alert->getAlertType()->getName() == "between") {
                if ($measurement->getValue() >= $alert->getJsonDecodedValue()["value"] AND $measurement->getValue() <= $alert->getJsonDecodedValue()["value2"]) {
                    $this->sendMail($alert->getPrototype(), $measurement, $alert);
                }
            } else if ($alert->getAlertType()->getName() == "greater") {
                if ($measurement->getValue() >= $alert->getJsonDecodedValue()["value"]) {
                    $this->sendMail($alert->getPrototype(), $measurement, $alert);
                }
            } else if ($alert->getAlertType()->getName() == "lower") {
                if ($measurement->getValue() <= $alert->getJsonDecodedValue()["value"]) {
                    $this->sendMail($alert->getPrototype(), $measurement, $alert);
                }
            }
            $this->em->detach($alert);
        }
    }

    private function sendMail(Prototype $prototype, Measurement $measurement, Alert $alert)
    {
        switch ($measurement->getType()->getName()) {
            case "battery":
                $this->checkBatteryValue($prototype, $measurement, $alert);
                break;
            case "hydrometric":
                $this->checkHydrometricValue($prototype, $measurement, $alert);
                break;
            case "rain":
                $this->checkRainValue($prototype, $measurement, $alert);
                break;
            case "temp":
                $this->checkTempValue($prototype, $measurement, $alert);
                break;
        }

    }

    private function checkBatteryValue(Prototype $prototype, Measurement $measurement, Alert $alert)
    {
        $checkSend = $this->em->getRepository('WaterWatchBundle:PersonalAlertSend')->findOneBy(["alert" => $alert->getId(), "measurement" => $measurement->getId()]);
        if (!$checkSend) {
            $this->output->writeln("invio email valore batteria personale a " . $alert->getUser()->getUsername());
            $this->mailer->sendEmail("Allerta Personalizzata Valore Batteria", $alert->getUser()->getMail(), "WaterWatchBundle:Mail/Personal:battery.html.twig", ["username" => $alert->getUser()->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "alertType" => $alert->getAlertType()->getName(), "alertValue" => $alert->getJsonDecodedValue()]);
            $alertSend = new PersonalAlertSend();
            $alertSend->setMeasurement($measurement);
            $alertSend->setAlert($alert);
            $alertSend->setData(new DateTime("now"));
            $this->em->persist($alertSend);
            $this->em->flush($alertSend);
        }

    }

    private function checkHydrometricValue(Prototype $prototype, Measurement $measurement, Alert $alert)
    {
        $checkSend = $this->em->getRepository('WaterWatchBundle:PersonalAlertSend')->findOneBy(["alert" => $alert, "measurement" => $measurement]);
        if (!$checkSend) {
            $this->output->writeln("invio email valore idrometrico personale a " . $alert->getUser()->getUsername());
            $this->mailer->sendEmail("Allerta Personalizzata Valore Idrometrico", $alert->getUser()->getMail(), "WaterWatchBundle:Mail/Personal:hydrometric.html.twig", ["username" => $alert->getUser()->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "alertType" => $alert->getAlertType()->getName(), "alertValue" => $alert->getJsonDecodedValue()]);
            $alertSend = new PersonalAlertSend();
            $alertSend->setMeasurement($measurement);
            $alertSend->setAlert($alert);
            $alertSend->setData(new DateTime("now"));
            $this->em->persist($alertSend);
            $this->em->flush($alertSend);
        }
    }

    private function checkRainValue(Prototype $prototype, Measurement $measurement, Alert $alert)
    {
        $checkSend = $this->em->getRepository('WaterWatchBundle:PersonalAlertSend')->findOneBy(["alert" => $alert, "measurement" => $measurement]);
        if (!$checkSend) {
            $this->output->writeln("invio email valore pioggia personale a " . $alert->getUser()->getUsername());
            $this->mailer->sendEmail("Allerta Personalizzata Valore Pioggia", $alert->getUser()->getMail(), "WaterWatchBundle:Mail/Personal:rain.html.twig", ["username" => $alert->getUser()->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "alertType" => $alert->getAlertType()->getName(), "alertValue" => $alert->getJsonDecodedValue()]);
            $alertSend = new PersonalAlertSend();
            $alertSend->setMeasurement($measurement);
            $alertSend->setAlert($alert);
            $alertSend->setData(new DateTime("now"));
            $this->em->persist($alertSend);
            $this->em->flush($alertSend);
        }
    }

    private function checkTempValue(Prototype $prototype, Measurement $measurement, Alert $alert)
    {
        $checkSend = $this->em->getRepository('WaterWatchBundle:PersonalAlertSend')->findOneBy(["alert" => $alert, "measurement" => $measurement]);
        if (!$checkSend) {
            $this->output->writeln("invio email valore temperatura personale a " . $alert->getUser()->getUsername());
            $this->mailer->sendEmail("Allerta Personalizzata Valore Temperatura", $alert->getUser()->getMail(), "WaterWatchBundle:Mail/Personal:temp.html.twig", ["username" => $alert->getUser()->getUsername(), "now" => $measurement->getValue(), "location" => $prototype->getLocation(), "alertType" => $alert->getAlertType()->getName(), "alertValue" => $alert->getJsonDecodedValue()]);
            $alertSend = new PersonalAlertSend();
            $alertSend->setMeasurement($measurement);
            $alertSend->setAlert($alert);
            $alertSend->setData(new DateTime("now"));
            $this->em->persist($alertSend);
            $this->em->flush($alertSend);
        }
    }

}
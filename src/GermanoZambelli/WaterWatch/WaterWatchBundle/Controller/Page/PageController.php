<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Controller\Page;

use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddAlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddPrototypeType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditAlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditPrototypeType;
use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{

    public function prototypesAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            /**
             * @var User $user ;
             */
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $prototypes = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findAll();
            if ($user->getIsAdmin() == 1) {
                return $this->render('@WaterWatch/Page/prototypes/prototypes.html.twig', ["prototypes" => $prototypes, "user" => $user]);
            } else {
                return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function prototypeAction($id)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            /**
             * @var User $user ;
             */
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $prototypeEntity = new Prototype();
            $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["id" => $id]);
            if ($user->getIsAdmin() == 1) {
                if ($prototype) {
                    $form = $this->createForm(EditPrototypeType::class, $prototypeEntity);
                    return $this->render('@WaterWatch/Page/prototypes/prototype.html.twig', ["form" => $form->createView(), "prototype" => $prototype, "user" => $user]);
                } else {
                    return $this->redirectToRoute('germano_zambelli_waterwatch_waterwatchbundle_prototypes_page');
                }
            } else {
                return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }


    public function addPrototypeAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            /**
             * @var User $user ;
             */
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            if ($user->getIsAdmin() == 1) {
                $prototype = new Prototype();
                $form = $this->createForm(AddPrototypeType::class, $prototype);
                $key = $this->get('germano_zambelli_waterwatch_waterwatch.prototype')->generateApiKey();
                return $this->render('@WaterWatch/Page/prototypes/addPrototype.html.twig', ["apiKey" => $key, "form" => $form->createView(), "user" => $user]);
            } else {
                return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function addAlertsAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $alert = new Alert();
            $form = $this->createForm(AddAlertType::class, $alert);
            return $this->render('@WaterWatch/Page/alerts/addAlert.html.twig', ["form" => $form->createView(), "user" => $user]);

        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function alertsAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $alerts = $this->getDoctrine()->getRepository('WaterWatchBundle:Alert')->findBy(["user" => $user->getId()]);
            return $this->render('@WaterWatch/Page/alerts/alerts.html.twig', ["alerts" => $alerts, "user" => $user]);
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function alertAction($id)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $alertEntity = new Prototype();
            $alert = $this->getDoctrine()->getRepository('WaterWatchBundle:Alert')->findOneBy(["id" => $id]);
            if ($alert) {
                $form = $this->createForm(EditAlertType::class, $alertEntity, ["data" => $alert]);
                return $this->render('@WaterWatch/Page/alerts/alert.html.twig', ["form" => $form->createView(), "alert" => $alert, "user" => $user]);
            } else {
                return $this->redirectToRoute('germano_zambelli_waterwatch_waterwatchbundle_prototypes_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }


}

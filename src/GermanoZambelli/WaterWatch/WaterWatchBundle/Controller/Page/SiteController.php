<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Controller\Page;

use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddAlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddPrototypeType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditAlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditPrototypeType;
use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SiteController extends Controller
{

    public function homepageAction()
    {
        return $this->render('@WaterWatch/Sites/Page/index.html.twig');
    }

    public function prototypesAction()
    {
        $prototypes = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findAll();
        return $this->render('@WaterWatch/Sites/Page/prototypes.html.twig', ["prototypes" => $prototypes, "googleApi" => $this->getParameter('api_google')]);
    }

    public function prototypeAction($id)
    {
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["id" => $id]);

        /**
         * @var Measurement $measurements
         */
        $measurements = $this->getDoctrine()->getRepository('WaterWatchBundle:Measurement')->findAll(["prototype" => $prototype]);
        $m = [];

        /**
         * @var Measurement $measurement
         */
        foreach ($measurements as $measurement){
            if(isset($m[$measurement->getData()->format("Y/m/d_H:i")])){
                if($measurement->getType()->getName() == "hydrometric"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["hydrometric"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "temp"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["temp"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "battery"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["battery"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "rain"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["rain"] = $measurement->getValue();
                }
            }else{
                $m[$measurement->getData()->format("Y/m/d_H:i")] = [];
                if($measurement->getType()->getName() == "hydrometric"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["hydrometric"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "temp"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["temp"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "battery"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["battery"] = $measurement->getValue();
                }elseif ($measurement->getType()->getName() == "rain"){
                    $m[$measurement->getData()->format("Y/m/d_H:i")]["rain"] = $measurement->getValue();
                }
            }
        }

        return $this->render('@WaterWatch/Sites/Page/prototype.html.twig', ["prototype" => $prototype, "measurements" => $m, "googleApi" => $this->getParameter('api_google')]);
    }

}


<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Controller\Ajax\Alert;

use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddAlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditAlertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AlertController extends Controller
{

    public function addAlertAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                $alertEntity = new Alert();
                $form = $this->createForm(AddAlertType::class, $alertEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $value2 = ($form['value2']->getData() != "") ? $form['value2']->getData() : "0";
                    $value = ($alertEntity->getAlertType()->getName() == "between") ? json_encode(["value" => $alertEntity->getValue(), "value2" => $value2]) : json_encode(["value" => $alertEntity->getValue()]);
                    $alert = $this->getDoctrine()->getRepository('WaterWatchBundle:Alert')->findOneBy(['prototype' => $alertEntity->getPrototype(), "measurementType" => $alertEntity->getMeasurementType(), "alertType" => $alertEntity->getAlertType(), "user" => $user, "value" => $value]);
                    if (!$alert) {
                        $alertEntity->setUser($user);
                        $alertEntity->setValue($value);
                        $this->getDoctrine()->getManager()->persist($alertEntity);
                        $this->getDoctrine()->getManager()->flush($alertEntity);
                        $response = ["success" => true, "message" => $this->get('translator')->trans('alerts.add.success')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('alerts.add.already_exist')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deleteAlertAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                if ($request->get('id') != "") {
                    $alert = $this->getDoctrine()->getRepository('WaterWatchBundle:Alert')->findOneBy(["id" => $request->get('id')]);
                    if ($alert) {
                        $this->getDoctrine()->getManager()->remove($alert);
                        $this->getDoctrine()->getManager()->flush($alert);
                        $response = ["success" => true, "message" => $this->get('translator')->trans('alerts.show.success_delete')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function editAlertAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $alertEntity = new Alert();
                $form = $this->createForm(EditAlertType::class, $alertEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $alert = $this->getDoctrine()->getRepository('WaterWatchBundle:Alert')->findOneBy(["id" => $form['id']->getData()]);
                    if ($alert) {
                        $value2 = ($form['value2']->getData() != "") ? $form['value2']->getData() : "0";
                        $value = ($alertEntity->getAlertType()->getName() == "between") ? json_encode(["value" => $form['value']->getData(), "value2" => $value2]) : json_encode(["value" => $form['value']->getData()]);
                        $alert->setPrototype($form['prototype']->getData());
                        $alert->setAlertType($form['alertType']->getData());
                        $alert->setMeasurementType($form['measurementType']->getData());
                        $alert->setValue($value);
                        $this->getDoctrine()->getManager()->flush($alert);
                        $response = ["success" => true, "message" => $this->get('translator')->trans('alert.success')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


}
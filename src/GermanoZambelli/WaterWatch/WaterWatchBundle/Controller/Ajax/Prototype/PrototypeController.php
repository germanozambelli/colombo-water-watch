<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Controller\Ajax\Prototype;

use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\AddPrototypeType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Form\EditPrototypeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class PrototypeController extends Controller
{

    public function addPrototypeAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                if ($user->getIsAdmin() == 1) {
                    $prototypeEntity = new Prototype();
                    $form = $this->createForm(AddPrototypeType::class, $prototypeEntity);
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $proto = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $request->get('apiKey')]);
                        if (!$proto) {
                            $prototypeEntity->setApiKey($request->get('apiKey'));
                            $this->getDoctrine()->getManager()->persist($prototypeEntity);
                            $this->getDoctrine()->getManager()->flush();
                            $response = ["success" => true, "message" => $this->get('translator')->trans('prototypes.add.success')];
                        } else {
                            $response = ["success" => false, "message" => $this->get('translator')->trans('prototypes.add.api_key_busy')];
                        }
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function editPrototypeAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                if ($user->getIsAdmin() == 1) {
                    $prototypeEntity = new Prototype();
                    $form = $this->createForm(EditPrototypeType::class, $prototypeEntity);
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $proto = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["id" => $form['id']->getData()]);
                        if ($proto) {
                            $proto->setLocation($form['location']->getData());
                            $proto->setBatteryAlert($form['batteryAlert']->getData());
                            $proto->setHydrometricAlert($form['hydrometricAlert']->getData());
                            $proto->setRainAlert($form['rainAlert']->getData());
                            $proto->setTempAlert($form['tempAlert']->getData());
                            $this->getDoctrine()->getManager()->flush($proto);
                            $response = ["success" => true, "message" => $this->get('translator')->trans('prototype.success')];
                        } else {
                            $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                        }
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deletePrototypeAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                if ($request->get('id') != "") {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                    if ($user->getIsAdmin() == 1) {
                        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["id" => $request->get('id')]);
                        if ($prototype) {
                            $this->getDoctrine()->getManager()->remove($prototype);
                            $this->getDoctrine()->getManager()->flush($prototype);
                            $response = ["success" => true, "message" => $this->get('translator')->trans('prototypes.show.success_delete')];
                        } else {
                            $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                        }
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
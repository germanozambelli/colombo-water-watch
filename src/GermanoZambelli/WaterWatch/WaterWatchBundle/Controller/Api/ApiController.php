<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ApiController extends Controller
{

    public function addMeasurementAction($apiKey, $measurement, Request $request)
    {
        $response = ["success" => false, "message" => "null"];
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $apiKey]);
        if ($prototype) {
            $measurement = json_decode(urldecode($measurement));

            $this->get('germano_zambelli_waterwatch_waterwatch.api')->addTempMeasurement($prototype, $measurement->temp, $request->getClientIp());
            $this->get('germano_zambelli_waterwatch_waterwatch.api')->addHydrometricMeasurement($prototype, $measurement->hydrometric, $request->getClientIp());
            $this->get('germano_zambelli_waterwatch_waterwatch.api')->addRainMeasurement($prototype, $measurement->rain, $request->getClientIp());
            $this->get('germano_zambelli_waterwatch_waterwatch.api')->addBatteryMeasurement($prototype, $measurement->battery, $request->getClientIp());
            $response = ["success" => true, "message" => "measurement successfully added"];

        } else {
            $response = ["success" => false, "message" => "invalid api key"];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addTempMeasurementAction($apiKey, $temp, Request $request)
    {
        $response = ["success" => false, "message" => "null"];
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $apiKey]);
        if ($prototype) {

            if ($temp) {
                $this->get('germano_zambelli_waterwatch_waterwatch.api')->addTempMeasurement($prototype, $temp, $request->getClientIp());
                $response = ["success" => true, "message" => "measurement successfully added"];
            } else {
                $response = ["success" => false, "message" => "invalid measurement data"];
            }
        } else {
            $response = ["success" => false, "message" => "invalid api key"];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addHydrometricMeasurementAction($apiKey, $hydrometric, Request $request)
    {
        $response = ["success" => false, "message" => "null"];
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $apiKey]);
        if ($prototype) {

            if ($hydrometric) {
                $this->get('germano_zambelli_waterwatch_waterwatch.api')->addHydrometricMeasurement($prototype, $hydrometric, $request->getClientIp());
                $response = ["success" => true, "message" => "measurement successfully added"];
            } else {
                $response = ["success" => false, "message" => "invalid measurement data"];
            }
        } else {
            $response = ["success" => false, "message" => "invalid api key"];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addBatteryMeasurementAction($apiKey, $battery, Request $request)
    {
        $response = ["success" => false, "message" => "null"];
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $apiKey]);
        if ($prototype) {
            if ($battery) {
                $this->get('germano_zambelli_waterwatch_waterwatch.api')->addBatteryMeasurement($prototype, $battery, $request->getClientIp());
                $response = ["success" => true, "message" => "measurement successfully added"];
            } else {
                $response = ["success" => false, "message" => "invalid measurement data"];
            }
        } else {
            $response = ["success" => false, "message" => "invalid api key"];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addRainMeasurementAction($apiKey, $rain, Request $request)
    {
        $response = ["success" => false, "message" => "null"];
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["apiKey" => $apiKey]);
        if ($prototype) {
            if ($rain) {
                $this->get('germano_zambelli_waterwatch_waterwatch.api')->addRainMeasurement($prototype, $rain, $request->getClientIp());
                $response = ["success" => true, "message" => "measurement successfully added"];
            } else {
                $response = ["success" => false, "message" => "invalid measurement data"];
            }
        } else {
            $response = ["success" => false, "message" => "invalid api key"];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getPrototypesAction()
    {
        $prototypes = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findAll();
        $proto = [];
        $i = 0;
        foreach ($prototypes as $prototype) {
            $proto[$i]["id"] = $prototype->getId();
            $proto[$i]["location"] = $prototype->getLocation();
            $proto[$i]["temp_alert"] = $prototype->getTempAlert();
            $proto[$i]["hydrometric_alert"] = $prototype->getHydrometricAlert();
            $proto[$i]["rain_alert"] = $prototype->getRainAlert();
            $proto[$i]["battery_alert"] = $prototype->getBatteryAlert();
            $i++;
        }
        $response = ["success" => true, "message" => $proto];
        $response = new Response(json_encode($response, true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getPrototypeAction($id)
    {
        $prototype = $this->getDoctrine()->getRepository('WaterWatchBundle:Prototype')->findOneBy(["id" => $id]);
        $proto = [];
        $proto["id"] = $prototype->getId();
        $proto["location"] = $prototype->getLocation();
        $proto["temp_alert"] = $prototype->getTempAlert();
        $proto["hydrometric_alert"] = $prototype->getHydrometricAlert();
        $proto["rain_alert"] = $prototype->getRainAlert();
        $proto["battery_alert"] = $prototype->getBatteryAlert();
        $response = ["success" => true, "message" => $proto];
        $response = new Response(json_encode($response, true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getMeasurementAction()
    {
        $measurements = $this->getDoctrine()->getRepository('WaterWatchBundle:Measurement')->findAll();

        $me = [];
        $i = 0;
        foreach ($measurements as $measurement) {
            $me[$i]["id"] = $measurement->getId();
            $me[$i]["prototype_id"] = $measurement->getPrototype()->getId();
            $me[$i]["type"] = $measurement->getType()->getName();
            $me[$i]["data"] = $measurement->getData()->format("d/m/Y H:i:s");
            $me[$i]["value"] = $measurement->getValue();
            $i++;
        }
        $response = ["success" => true, "message" => $me];
        $response = new Response(json_encode($response, true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getMeasurementByTypeAction($type)
    {
        $type = $this->getDoctrine()->getRepository('WaterWatchBundle:MeasurementType')->findOneBy(["name" => $type])->getId();
        $measurements = $this->getDoctrine()->getRepository('WaterWatchBundle:Measurement')->findBy(["type" => $type]);
        $me = [];
        $i = 0;
        foreach ($measurements as $measurement) {
            $me[$i]["id"] = $measurement->getId();
            $me[$i]["prototype_id"] = $measurement->getPrototype()->getId();
            $me[$i]["type"] = $measurement->getType()->getName();
            $me[$i]["data"] = $measurement->getData()->format("d/m/Y H:i:s");
            $me[$i]["value"] = $measurement->getValue();
            $i++;
        }
        $response = ["success" => true, "message" => $me];
        $response = new Response(json_encode($response, true));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
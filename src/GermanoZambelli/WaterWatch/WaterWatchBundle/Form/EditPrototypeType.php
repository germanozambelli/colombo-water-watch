<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class EditPrototypeType extends AbstractType
{
    /**
     * @var Router
     */
    private $router;

    /**
     * LoginType constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, ['mapped' => false, 'constraints' => [new NotBlank(), new NotNull()], 'attr' => ['hidden' => 'hidden', 'class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('location', TextType::class, ['constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('apiKey', TextType::class, ['constraints' => [], 'attr' => ['disabled' => 'disabled', 'class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('tempAlert', NumberType::class, ['constraints' => [], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('hydrometricAlert', NumberType::class, ['constraints' => [], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('rainAlert', NumberType::class, ['constraints' => [], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('batteryAlert', NumberType::class, ['constraints' => [], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('edit', SubmitType::class, ["label" => 'edit', 'attr' => ['value' => 'edit', 'class' => 'btn btn-primary pull-right']])
            ->setMethod('POST')
            ->setAction($this->router->generate('germano_zambelli_waterwatch_waterwatchbundle_ajax_prototypes_edit'));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype']);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'editprototype';
    }


}

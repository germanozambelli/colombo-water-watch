<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertType;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;
use \GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class AddAlertType extends AbstractType
{
    /**
     * @var Router
     */
    private $router;

    /**
     * LoginType constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prototype', EntityType::class, ['class' => 'GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype', 'choice_label' => function (Prototype $prototype) {
                return $prototype->getLocation();
            }, 'constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('measurementType', EntityType::class, ['class' => 'GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\MeasurementType', 'choice_label' => function (MeasurementType $type) {
                return $type->getName();
            }, 'constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control'], 'required' => false, 'choice_translation_domain' => true, 'translation_domain' => 'messages'])
            ->add('alertType', EntityType::class, ['class' => 'GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\AlertType', 'choice_label' => function (AlertType $type) {
                return $type->getName();
            }, 'constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control'], 'required' => false, 'choice_translation_domain' => true, 'translation_domain' => 'messages'])
            ->add('value', NumberType::class, ['constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('value2', NumberType::class, ["mapped" => false, 'attr' => ['class' => 'form-control'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('add', SubmitType::class, ["label" => 'add', 'attr' => ['value' => 'add', 'class' => 'btn btn-primary pull-right']])
            ->setMethod('POST')
            ->setAction($this->router->generate('germano_zambelli_waterwatch_waterwatchbundle_ajax_alerts_add'));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Alert']);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'addalert';
    }


}

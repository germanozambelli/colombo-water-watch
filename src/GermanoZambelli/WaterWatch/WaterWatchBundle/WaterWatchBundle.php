<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */
namespace GermanoZambelli\WaterWatch\WaterWatchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class WaterWatchBundle extends Bundle
{
    /**
     * WaterWatchBundle constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        if (!class_exists('GermanoZambelli\\ZMBManagement\\CoreBundle\\CoreBundle', false)) {
            throw new \Exception('WaterWatchBundle require ZMBManagement CoreBundle');
        }

        if (!class_exists('GermanoZambelli\\ZMBManagement\\UserBundle\\UserBundle', false)) {
            throw new \Exception('WaterWatchBundle require ZMBManagement UserBundle');
        }
    }

}

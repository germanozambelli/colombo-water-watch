<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Services;

use Swift_Mailer;
use Swift_Message;
use Swift_Mime_Message;
use Swift_Spool;
use Swift_Transport_EsmtpTransport;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MailServices
{
    /**
     * @var Swift_Mailer $mailer ;
     */
    protected $mailer;

    /**
     * @var Swift_Spool $spool
     */
    protected $spool;

    /**
     * @var Swift_Transport_EsmtpTransport $realtransport
     */
    protected $realtransport;

    /**
     * @var TwigEngine $container
     */
    protected $templating;

    public function __construct(Swift_Mailer $mailer, Swift_Spool $spool, Swift_Transport_EsmtpTransport $realtransport, $templating)
    {
        $this->mailer = $mailer;
        $this->spool = $spool;
        $this->realtransport = $realtransport;
        $this->templating = $templating;
    }

    public function sendEmail($subject, $to, $views, $viewsoption)
    {
        /**
         * @var Swift_Mime_Message $message
         */
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setBody($this->templating->render($views, $viewsoption),
                'text/html');
        try {
            $this->realtransport->start();
            $this->mailer->send($message);
            $this->realtransport->stop();
        } catch (\Exception $exc) {
            new Exception($exc->getTraceAsString());
        }

        var_dump($this->spool->flushQueue($this->realtransport));
    }
}
<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Services;

use Doctrine\ORM\EntityManager;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Measurement;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;

class ApiServices
{
    /**
     * This var contain the Entity Manager for access to doctrine.
     *
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addTempMeasurement(Prototype $prototype, $value, $ip)
    {
        $measurement = new Measurement();
        $measurement->setData(new \DateTime("now"));
        $measurement->setIp($ip);
        $value = $value / 100;
        $measurement->setValue($value);
        $measurement->setPrototype($prototype);
        $measurement->setType($this->em->getRepository('WaterWatchBundle:MeasurementType')->findOneBy(["name" => "temp"]));
        $this->em->persist($measurement);
        $this->em->flush();
    }

    public function addBatteryMeasurement(Prototype $prototype, $value, $ip)
    {
        $measurement = new Measurement();
        $measurement->setData(new \DateTime("now"));
        $measurement->setIp($ip);
        $value = $value / 100;
        $measurement->setValue($value);
        $measurement->setPrototype($prototype);
        $measurement->setType($this->em->getRepository('WaterWatchBundle:MeasurementType')->findOneBy(["name" => "battery"]));
        $this->em->persist($measurement);
        $this->em->flush();
    }

    public function addHydrometricMeasurement(Prototype $prototype, $value, $ip)
    {
        $measurement = new Measurement();
        $measurement->setData(new \DateTime("now"));
        $measurement->setIp($ip);
        $value = $value / 100;
        $measurement->setValue($value);
        $measurement->setPrototype($prototype);
        $measurement->setType($this->em->getRepository('WaterWatchBundle:MeasurementType')->findOneBy(["name" => "hydrometric"]));
        $this->em->persist($measurement);
        $this->em->flush();
    }

    public function addRainMeasurement(Prototype $prototype, $value, $ip)
    {
        $measurement = new Measurement();
        $measurement->setData(new \DateTime("now"));
        $measurement->setIp($ip);
        $value = $value / 100;
        $measurement->setValue($value);
        $measurement->setPrototype($prototype);
        $measurement->setType($this->em->getRepository('WaterWatchBundle:MeasurementType')->findOneBy(["name" => "rain"]));
        $this->em->persist($measurement);
        $this->em->flush();
    }

}
<?php

/**
 *    ______      __                __             _       __      __               _       __      __       __
 *   / ____/___  / /___  ____ ___  / /_  ____     | |     / /___ _/ /____  _____   | |     / /___ _/ /______/ /_
 *  / /   / __ \/ / __ \/ __ `__ \/ __ \/ __ \    | | /| / / __ `/ __/ _ \/ ___/   | | /| / / __ `/ __/ ___/ __ \
 * / /___/ /_/ / / /_/ / / / / / / /_/ / /_/ /    | |/ |/ / /_/ / /_/  __/ /       | |/ |/ / /_/ / /_/ /__/ / / /
 * \____/\____/_/\____/_/ /_/ /_/_.___/\____/     |__/|__/\__,_/\__/\___/_/        |__/|__/\__,_/\__/\___/_/ /_/
 *
 *
 *
 *  Copyright (C) 2017 IIS COLOMBO SANREMO - Germano Zambelli <zamba.germano@hotmail.it> - Marco Caruso - Riccardo Breccione - Andrea Nolli
 *
 */

namespace GermanoZambelli\WaterWatch\WaterWatchBundle\Services;


use DateTime;
use Doctrine\ORM\EntityManager;
use GermanoZambelli\WaterWatch\WaterWatchBundle\Entity\Prototype;


class MyTwigServices extends \Twig_Extension
{

    /**
     * This var contain the Entity Manager for access to doctrine.
     *
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('prototypeIsOnline', array($this, 'prototypeIsOnlineFilter')),
            new \Twig_SimpleFilter('parseDate', array($this, 'parseDate')),
        );
    }

    public function prototypeIsOnlineFilter(Prototype $prototype)
    {
        $measurement = $this->em->getRepository('WaterWatchBundle:Measurement')->findOneBy(["prototype" => $prototype]);
        $now = new DateTime("now");
        if($measurement){
            if(($now->getTimestamp() - $measurement->getData()->getTimestamp()) <= 600) {
                return true;
            }else{
                return falsE;
            }
        }else{
            return false;
        }
    }

    public function parseDate($date){
        $date = explode("_",$date);
        $date1 =  explode("/", $date[0]);
        return $date1[2]."/".$date1[1]."/".$date1[0]." ".$date[1];
    }

}
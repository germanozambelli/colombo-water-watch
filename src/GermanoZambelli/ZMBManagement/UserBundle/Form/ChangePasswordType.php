<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */


namespace GermanoZambelli\ZMBManagement\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ChangePasswordType extends AbstractType
{
    /**
     * @var Router
     */
    private $router;

    /**
     * LoginType constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'change_password.password_match',
                'options' => array('constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control']),
                'required' => false,
                'first_options' => array('label' => 'change_password.password'),
                'second_options' => array('label' => 'change_password.repeat_password'),
            ))
            ->add('send', SubmitType::class, ["label" => 'edit', 'attr' => ['value' => 'edit', 'class' => 'btn btn-primary pull-right']])
            ->setMethod('POST')
            ->setAction($this->router->generate('germano_zambelli_zmbmanagement_userbundle_ajax_account_change_password'));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'GermanoZambelli\ZMBManagement\UserBundle\Entity\User']);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'changepassword';
    }


}

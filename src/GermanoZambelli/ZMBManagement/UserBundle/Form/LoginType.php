<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */

namespace GermanoZambelli\ZMBManagement\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class LoginType extends AbstractType
{
    /**
     * @var Router
     */
    private $router;

    /**
     * LoginType constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control', 'placeholder' => 'login.username'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('password', PasswordType::class, ['constraints' => [new NotBlank(), new NotNull()], 'attr' => ['class' => 'form-control', 'placeholder' => 'login.password'], 'required' => false, 'translation_domain' => 'messages'])
            ->add('send', SubmitType::class, ["label" => 'login.send', 'attr' => ['value' => 'login.send', 'class' => 'btn btn-primary btn-block btn-flat']])
            ->setMethod('POST')
            ->setAction($this->router->generate('germano_zambelli_zmbmanagement_userbundle_ajax_login_check'));
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'GermanoZambelli\ZMBManagement\UserBundle\Entity\User']);
    }


    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'login';
    }


}

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */


$('form[name="login"]').on('submit', (function (e) {
    e.preventDefault();
    $.ajax({
        url: this.getAttribute('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (msg) {
            if (msg.success == true) {
                location.reload();
            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}));


$('form[name="registeruser"]').on('submit', (function (e) {
    e.preventDefault();
    $.ajax({
        url: this.getAttribute('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (msg) {
            if (msg.success == true) {
                location.reload();
            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}));

$('form[name="changepassword"], form[name="changemail"], form[name="edituser"]').on('submit', (function (e) {
    e.preventDefault();
    $.ajax({
        url: this.getAttribute('action'),
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (msg) {
            if (msg.success == true) {
                $("div#msgSuccess").show();
                $("div#msgSuccess p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgSuccess").hide();
                }, 4000);
            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}));

function deleteUser(id, url) {
    $.ajax({
        url: decodeURIComponent(url),
        type: "POST",
        data: "id=" + id,
        cache: false,
        success: function (msg) {
            if (msg.success == true) {
                $("div#msgSuccess").show();
                $("div#msgSuccess p#text").html(msg.message);
                setTimeout(function () {
                    location.reload();
                    $("div#msgSuccess").hide();
                }, 2000);

            } else {
                $("div#msgError").show();
                $("div#msgError p#text").html(msg.message);
                setTimeout(function () {
                    $("div#msgError").hide();
                }, 4000);
            }
        },
        error: function () {
            console.log("call error");
        }
    });
}
<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */

namespace GermanoZambelli\ZMBManagement\UserBundle\Controller\Ajax\Login;

use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use GermanoZambelli\ZMBManagement\UserBundle\Form\LoginType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if (!$session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $userEntity = new User();
                $form = $this->createForm(LoginType::class, $userEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $form['username']->getData(), "password" => $userEntity->getEncryptedPassword($form['password']->getData())]);
                    if ($user) {
                        $session->set('username', $user->getUsername());
                        $response = ["success" => true, "message" => $this->get('translator')->trans('login.already_logged_in')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('login.user_not_found')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('login.already_logged_in')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function registerAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if (!$session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $userEntity = new User();
                $form = $this->createForm(RegisterType::class, $userEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $form['username']->getData()]);
                    if (!$user) {
                        $userEntity->setIsAdmin("0");
                        $this->getDoctrine()->getManager()->persist($userEntity);
                        $this->getDoctrine()->getManager()->flush();
                        $session->set('username', $userEntity->getUsername());
                        $response = ["success" => true, "message" => $this->get('translator')->trans('register.success')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('register.busy_username')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('login.already_logged_in')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
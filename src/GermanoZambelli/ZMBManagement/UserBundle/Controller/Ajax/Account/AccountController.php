<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */

namespace GermanoZambelli\ZMBManagement\UserBundle\Controller\Ajax\Account;

use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use GermanoZambelli\ZMBManagement\UserBundle\Form\ChangePasswordType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\LoginType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\MailType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class AccountController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function changeMailAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $userEntity = new User();
                $form = $this->createForm(MailType::class, $userEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                    $user->setMail($form['mail']->getData());
                    $this->getDoctrine()->getManager()->flush($user);
                    $response = ["success" => true, "message" => $this->get('translator')->trans('change_mail.success')];
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function changePasswordAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $userEntity = new User();
                $form = $this->createForm(ChangePasswordType::class, $userEntity);
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                    $user->setPassword($form['password']->getData());
                    $this->getDoctrine()->getManager()->flush($user);
                    $response = ["success" => true, "message" => $this->get('translator')->trans('change_password.success')];
                } else {
                    if ($this->get('validator')->validate($form)[0]->getMessage() == "change_password.password_match") {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('change_password.password_match')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                    }
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
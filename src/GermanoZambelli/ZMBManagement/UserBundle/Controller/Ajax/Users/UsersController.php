<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */

namespace GermanoZambelli\ZMBManagement\UserBundle\Controller\Ajax\Users;

use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use GermanoZambelli\ZMBManagement\UserBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class UsersController extends Controller
{

    public function deleteUserAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                if ($request->get('id') != "") {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                    if ($user->getIsAdmin() == 1 AND $user->getId() != $request->get('id')) {
                        $u = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["id" => $request->get('id')]);
                        $this->getDoctrine()->getManager()->remove($u);
                        $this->getDoctrine()->getManager()->flush($u);
                        $response = ["success" => true, "message" => $this->get('translator')->trans('users.show.success_delete')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function editUserAction(Request $request)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            if ($request->isXmlHttpRequest()) {
                $userEntity = new User();
                $form = $this->createForm(UserType::class, $userEntity);
                $form->handleRequest($request);
                if ($form['id']->getData() != "" AND $form->isValid()) {
                    $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
                    if ($user->getIsAdmin() == 1) {
                        $u = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["id" => $form['id']->getData()]);
                        $u->setMail($form['mail']->getData());
                        $u->setIsAdmin($form['isAdmin']->getData());
                        if ($form['password'] != "") {
                            $u->setPassword($form['password']->getData());
                        }
                        $this->getDoctrine()->getManager()->flush($u);
                        $response = ["success" => true, "message" => $this->get('translator')->trans('user.success_edit')];
                    } else {
                        $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
                    }
                } else {
                    $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
                }
            } else {
                $response = ["success" => false, "message" => $this->get('translator')->trans('form_is_not_valid')];
            }
        } else {
            $response = ["success" => false, "message" => $this->get('translator')->trans('session_error')];
        }
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
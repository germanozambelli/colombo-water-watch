<?php

/**
 *   _______  __ ____    __  __                                                   _
 *  |__  |  \/  | __ )  |  \/  | __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
 *    / /| |\/| |  _ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ | '_ ` _ \ / _ | '_ \| __|
 *   / /_| |  | | |_) | | |  | | (_| | | | | (_| | (_| |  __| | | | | |  __| | | | |_
 *  /____|_|  |_|____/  |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
 *                                                |___/
 *
 *  Copyright (C) 2017 Germano Zambelli <zamba.germano@hotmail.it>
 */

namespace GermanoZambelli\ZMBManagement\UserBundle\Controller\Page;

use GermanoZambelli\ZMBManagement\UserBundle\Entity\User;
use GermanoZambelli\ZMBManagement\UserBundle\Form\ChangePasswordType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\LoginType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\MailType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\MailUserType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\RegisterType;
use GermanoZambelli\ZMBManagement\UserBundle\Form\UserType;
use GermanoZambelli\ZMBManagement\UserBundle\UserBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class PageController extends Controller
{
    /**
     * @return RedirectResponse|Response
     */
    public function loginAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if (!$session->get('username')) {
            $user = new User();
            $form = $this->createForm(LoginType::class, $user);
            return $this->render('@User/Page/login.html.twig', ["form" => $form->createView()]);
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_home_page');
        }
    }

    public function registerAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();

        if (!$session->get('username')) {
            $user = new User();
            $form = $this->createForm(RegisterType::class, $user);
            return $this->render('UserBundle:Page:register.html.twig', ["form" => $form->createView()]);
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_home_page');
        }
    }

    public function logoutAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $session->remove("username");
            $session->clear();
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_home_page');
        }
    }

    public function dashboardAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            return $this->render('@User/Page/dashboard.html.twig', ["user" => $user]);
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function accountAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $nuser = new User();
            $mailForm = $this->createForm(MailType::class, $nuser);
            $passwordForm = $this->createForm(ChangePasswordType::class, $nuser);
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            return $this->render('@User/Page/account.html.twig', ["mailForm" => $mailForm->createView(), "user" => $user, "passwordForm" => $passwordForm->createView()]);
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function usersAction()
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            /**
             * @var User $user ;
             */
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $users = $this->getDoctrine()->getRepository('UserBundle:User')->findAll();
            if ($user->getIsAdmin() == 1) {
                return $this->render('@User/Page/users/users.html.twig', ["users" => $users, "user" => $user]);
            } else {
                return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

    public function userAction($id)
    {
        $session = $this->get('request_stack')->getCurrentRequest()->getSession();
        if ($session->get('username')) {
            $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["username" => $session->get('username')]);
            $u = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(["id" => $id]);
            if ($user->getIsAdmin() == 1) {
                if ($u) {
                    $nuser = new User();
                    $editForm = $this->createForm(UserType::class, $nuser, ["data" => $u]);
                    return $this->render('@User/Page/users/user.html.twig', ["u" => $u, "user" => $user, "form" => $editForm->createView()]);
                } else {
                    return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_users_page');
                }
            } else {
                return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
            }
        } else {
            return $this->redirectToRoute('germano_zambelli_zmbmanagement_userbundle_login_page');
        }
    }

}
